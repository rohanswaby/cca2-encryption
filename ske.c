#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <gmp.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C | IV) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define IV_LEN 16
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	setSeed(entropy, entLen);
	randBytes(K->aesKey, 32);
	randBytes(K->hmacKey, 32);
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	int nWritten;

	/* Copy or generate IV */
	IV ? memset(outBuf, IV, IV_LEN) : randBytes(outBuf, IV_LEN);

	/* Encrypt plaintext with AES */
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,outBuf);
	EVP_EncryptUpdate(ctx,outBuf + IV_LEN,&nWritten,inBuf,len);
	EVP_CIPHER_CTX_free(ctx);

	/* Generate MAC */
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, outBuf, len + 16, outBuf + len + 16, NULL);
	return nWritten; 
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	struct stat st;
	stat(fnin, &st);
	size_t msg_len = st.st_size;
	int nWritten;

	int fnind = open(fnin, O_RDONLY, 0755);
	unsigned char *mmap_in = mmap(NULL, msg_len, PROT_READ, MAP_SHARED, fnind, 0);
	int fnoutd = open(fnout, O_RDWR | O_CREAT, 0755);
	int ret = ftruncate(fnoutd, ske_getOutputLen(msg_len));
	unsigned char *mmap_out = mmap(NULL, ske_getOutputLen(msg_len), PROT_READ | PROT_WRITE, MAP_SHARED, fnoutd, 0);

	/* Copy or generate IV */
	IV ? memcpy(mmap_out, IV, IV_LEN) : randBytes(mmap_out, IV_LEN);

	/* Encrypt plaintext with AES */
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,mmap_out);
	EVP_EncryptUpdate(ctx,mmap_out + IV_LEN,&nWritten,mmap_in,msg_len);
	EVP_CIPHER_CTX_free(ctx);

	/* Generate MAC */
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, mmap_out, msg_len + 16, mmap_out + msg_len + 16, NULL);

	msync(mmap_out, ske_getOutputLen(msg_len), MS_SYNC);
	munmap(mmap_in, msg_len);
	munmap(mmap_out, ske_getOutputLen(msg_len));
	close(fnind);
	close(fnoutd);
	return nWritten;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* verify MAC */
	unsigned char md[HM_LEN] = {0};
	HMAC(EVP_sha256(),K->hmacKey, HM_LEN, inBuf, len - HM_LEN, md,NULL);
	if (strncmp(inBuf + len - HM_LEN, md, HM_LEN)) {
		return -1; // hashes don't match
	}

	/* Decrypt message */
	int nWritten;
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,inBuf);
	EVP_DecryptUpdate(ctx,outBuf,&nWritten,inBuf + IV_LEN, len - IV_LEN - HM_LEN);
	EVP_CIPHER_CTX_free(ctx);
	return nWritten;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	struct stat st;
	stat(fnin, &st);
	size_t ct_len = st.st_size;
	size_t msg_len = ct_len - HM_LEN - IV_LEN;
	int nWritten;

	int fnind = open(fnin, O_RDONLY, 0);
	unsigned char *mmap_in = mmap(NULL, ct_len, PROT_READ, MAP_SHARED, fnind, 0);

	int fnoutd = open(fnout, O_RDWR | O_CREAT, 0755);
	int ret = ftruncate(fnoutd, msg_len);
	unsigned char *mmap_out = mmap(NULL, msg_len, PROT_READ | PROT_WRITE, MAP_SHARED, fnoutd, 0);

	/* Generate MAC */
	unsigned char md[HM_LEN];
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, mmap_in, msg_len + IV_LEN, md, NULL);
	if (!strncpy(md, mmap_in + msg_len + IV_LEN, HM_LEN)) {
		return -1;
	}

	/* Decrypt plaintext with AES */
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,mmap_in);
	EVP_EncryptUpdate(ctx,mmap_out,&nWritten,mmap_in + IV_LEN,msg_len);
	EVP_CIPHER_CTX_free(ctx);

	msync(mmap_out, msg_len, MS_SYNC);
	munmap(mmap_in, ct_len);
	munmap(mmap_out, msg_len);
	close(fnind);
	close(fnoutd);
	return nWritten;
}

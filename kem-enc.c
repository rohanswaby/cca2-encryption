/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
//#include <openssl/aes.h>
#include "ske.h"
#include "rsa.h"
#include "prf.h"
#include <sys/mman.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and ciphertext;
	 * write to fnOut. */
	// C = (Epk(x), H(x)), KDF(x) is the key

	size_t keylen = rsa_numBytesN(K);

	// Allocate space for stuff
	// for SK 
	unsigned char* x = malloc(keylen);
	// for SHA256
	unsigned char* sha256 = malloc(HASHLEN);
	// for RSA ciphertext and SHA256 (KEM according to NOTE above)
	unsigned char* rsa_cipher = malloc(keylen + HASHLEN);
	// for SKE encrypt
	unsigned char* IV = malloc(16);

	// Generate SK
	SKE_KEY SK;
	ske_keyGen(&SK, x, keylen);

	// SHA256
	SHA256(x, keylen, sha256);

	// RSA
	rsa_encrypt(rsa_cipher, x, keylen, K);

	// Put SHA256 and RSA together
	memcpy(rsa_cipher + keylen, sha256, HASHLEN);	// should look like SHA256(x)|RSA(x)

	// Write output file
	int outFile;
	outFile = open(fnOut, O_RDWR|O_CREAT, S_IRWXU);
	if(outFile < 0)
	{
		perror("Error - Open failed.");
		return 1;
	}

	// Close file
	close(outFile);

	// SK Encrypt
	// Randomize IV
	randBytes(IV, 16);
	ske_encrypt_file(fnOut, fnIn, &SK, IV, keylen + HASHLEN);

	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	// Decapsulation algorithm on inpurt C = (c0, c1) simply computes
	// x = Dpk(c0) and outputs KDF(x) if H(x) = c1, else ?

	/* 
		Step 1 is basically decrypt RSA(x), we get back x'
		Step 2 is SHA256(x') =? SHA256(x)
		Step 3 is do ske_keyGen again with x' and then decrypt with that SK
	*/

	// Variables for stuff
	// for in file
	int inFile;
	// for mmap
	unsigned char* mapFile;
	size_t fileSize;
	struct stat st;

	// Read in file
	inFile = open(fnIn, O_RDONLY);
	if(inFile < 0)
	{
		perror("Error - Open failed.");
		return 1;
	}

	// File Size
	stat(fnIn, &st);
	fileSize = st.st_size;

	// Copies file into buffer
	mapFile = mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, inFile, 0);
	if(mapFile == MAP_FAILED)
	{
		perror("Error - MMAP failed.");
		return 1;
	}
	close(inFile);

	// Step 1 - Decrypt RSA(x) to get x'
	// Size of RSA key
	size_t keylen = rsa_numBytesN(K);
	// Space to store x'
	unsigned char* x = malloc(keylen);
	// Copy into buffer
	memcpy(x, mapFile, keylen);
	// Decrypt
	unsigned char* decFile = malloc(keylen);
	rsa_decrypt(decFile, x, keylen, K);

	// Step 2 - SHA256(x') =? SHA256(x)
	// Allocate space for SHA256(x')
	unsigned char* sha256 = malloc(HASHLEN);
	// SHA256'
	SHA256(decFile, keylen, sha256);
	// Allocate space for correct SHA256 to compare
	unsigned char* correct_sha256 = malloc(HASHLEN);
	memcpy(correct_sha256, mapFile + keylen, HASHLEN);
	// Compare SHA256' with correct SHA256
	for(size_t i = 0; i < HASHLEN; i++)
	{
		if(sha256[i] != correct_sha256[i])
		{
			perror("Error - Different Hash.");
			return 1;
		}
	}

	// Step 3 is do ske_keyGen again with x' and then decrypt with that SK
	// Generate SK Key
	SKE_KEY SK;
	ske_keyGen(&SK, decFile, keylen);
	ske_decrypt_file(fnOut, fnIn, &SK, keylen + HASHLEN);

	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */

	// RSA Key
	RSA_KEY K;

	// Files
	FILE* rsa_pub_key;
	FILE* rsa_pri_key;

	switch (mode) {
		case ENC:
			// Get Public Key
			rsa_pub_key = fopen(fnKey,"r");
			if(rsa_pub_key == NULL)
			{
				perror("Error - Null Public RSA Key.");
				return 1;
			}

			// Write Key
			rsa_readPublic(rsa_pub_key, &K);
			
			// Close File
			fclose(rsa_pub_key);

			// Encrypt
			kem_encrypt(fnOut, fnIn, &K);
			

			// Shred Key
			rsa_shredKey(&K);
	
			break;
		case DEC:
			// Get Private Key
			rsa_pri_key = fopen(fnKey,"r");
			if(rsa_pri_key == NULL)
			{
				perror("Error - Null Private RSA Key.");
				return 1;
			}

			// Write Key
			rsa_readPrivate(rsa_pri_key, &K);
			
			// Close File
			fclose(rsa_pri_key);

			// Decrypt
			kem_decrypt(fnOut, fnIn, &K);

			// Shred Key
			rsa_shredKey(&K);

			break;
		case GEN:
			// Generate
			rsa_keyGen(nBits, &K);

			// Create file
			rsa_pri_key = fopen(fnOut, "w+");
			if(rsa_pri_key == NULL)
			{
				perror("Error - Null Private RSA Key.");
				return 1;
			}

			// Write Private Key
			rsa_writePrivate(rsa_pri_key, &K);

			// .pub
			strcat(fnOut, ".pub");

			// Create file
			rsa_pub_key = fopen(fnOut, "w+");
			if(rsa_pub_key == NULL)
			{
				perror("Error - Null Public RSA Key.");
				return 1;
			}

			// Write Public Key
			rsa_writePublic(rsa_pub_key, &K);

			// Close files
			fclose(rsa_pri_key);
			fclose(rsa_pub_key);

			// Shred Key
			rsa_shredKey(&K);

			break;
		default:
			return 1;
	}

	return 0;
}
